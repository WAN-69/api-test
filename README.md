
# API Test Repository

Repository ini ditujukan hanya untuk test PT 62 Teknologi


## Setup

Clone the project

```bash
  git clone https://gitlab.com/WAN-69/api-test.git
```

Go to the project directory

```bash
  cd api-test
```

Install dependencies

```bash
  composer install
```

Get the key app

```bash
  php artisan generate:key
```

Migrate database and seed data

```bash
  php artisan migrate --seed
```

Start the server

```bash
  php artisan serve
```

