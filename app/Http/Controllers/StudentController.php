<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $students = Student::when($request->has('search'),fn($query) => $query->where('name','like',"%{$request->search}%"))->orderBy($request->sortBy,$request->orderBy)->paginate(5);
        return response()->json([
            'message' => 'Sukses',
            'data' => $students
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Validator = Validator::make($request->all(),[
            'avatar' => 'required|image',
            'name' => 'required|string|unique:students,name|max:255',
            'gender' => 'required|max:255',
            'dob' => 'required|date',
        ]);

        if ($Validator->fails()) {
            return response()->json([
                'error' => $Validator->errors()
            ], 422);
        }
        
        $fileName = Str::slug('avatar'.'-'.$request->name.'-'.now()).'.'.$request->file('avatar')->extension();
        $path = $request->file('avatar')->storeAs('public/avatars',$fileName);
        
        $validatedData = collect($Validator->validate())->merge(['avatar' => $path])->toArray();

        $student = Student::create($validatedData);

        return response()->json([
            'status' => 'Sukses',
            'message' => 'Data Berhasil Disimpan',
            'data' => $student,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $data = Student::findorfail($id);
            return response()->json($data, 200);
        } catch (ModelNotFoundException $e) {
            return response([
                'status' => 'error',
                'error' => 'Data tidak ditemukan'
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Validator = Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'gender' => 'required|max:255',
            'dob' => 'required|date',
        ]);

        if ($Validator->fails()) {
            return response()->json([
                'error' => $Validator->errors()
            ], 422);
        }

        if ($request->hasFile('avatar')) {
            $fileName = Str::slug('avatar'.'-'.$request->name.'-'.now()).'.'.$request->file('avatar')->extension();
            $path = $request->file('avatar')->storeAs('public/avatars',$fileName);
        }

        $validatedData = $request->hasFile('avatar') ? collect($Validator->validate())->merge(['avatar' => $path])->toArray() : $Validator->validate();

        $student = Student::where('id', $id)->update($validatedData);

        return response()->json([
            'status' => 'Sukses',
            'message' => 'Data Berhasil Diupdate',
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Student::findorfail($id)->delete();
            return response()->json([
                'message' => 'Data Berhasil Dihapus'
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response([
                'status' => 'error',
                'error' => 'Data tidak ditemukan'
            ], 404);
        }
    }
}
